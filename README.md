# KNMI HAPI server

This project implements a HAPI server that complies to a very large extent with the specifications given in:

https://github.com/hapi-server/data-specification/tree/master/hapi-3.1.0

HAPI is the Heliophysics Application Programmer’s Interface (HAPI) and is mainly used for giving access to heliophysics and spaceweather time series data. The data available via HAPI include a large variety of observations of the space environment, but output of space environment models, or spacecraft locations in various coordinate frames are also included.

## HAPI server extensions
The KNMI HAPI server has a few extensions on top of the HAPI 3.1.0 specification.

- The server has not only the functionality to access data, but also to define new datasets, store data and remove data using the /api endpoint.
For this, a full REST API has been implemented, which in addition to GET requests, offers endpoints for PUT, POST and DELETE requests.
- The server has been extended with the functionality to define relations between datasets through the use of the x_relations keyword in the dataset info/metadata.

## Dataset relations
A specific relation is the resampled relation, where data is resampled to a different cadance for access of data over a large time span when a limited data rate is desirable. The server provides the possibility to automatically store data in datasets with the resampled relation.

Another implemented relation is the link relation which can be compared to a symbolic link in a Linux file system.
With this relation one can define that the data is not actually stored in the dataset but in the dataset pointed to by the link relation.
This can also be on another server.
A use case for this is where a large dataset is stored on a remote server, and resampled datasets are stored on the local server.

## Acknowledgements
The code has been maintained and expanded with support from ESA through the Swarm Data Innovation and Science Cluster (Swarm DISC). For more information on Swarm DISC, please visit https://earth.esa.int/eogateway/activities/swarm-disc
