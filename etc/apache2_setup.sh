cp /var/www/knmi-hapi-server/etc/apache2.conf /etc/apache2/apache2.conf
cp /var/www/knmi-hapi-server/etc/000-default.conf /etc/apache2/sites-enabled/000-default.conf
cp /var/www/knmi-hapi-server/etc/hapi_server.cfg /var/www/knmi-hapi-server/hapi_server.cfg
mkdir /data
#python3 /var/www/knmi-hapi-server/hapi/hapi_init.py
chown -R www-data:www-data /var/www/knmi-hapi-server
