# build the docker image
$ docker build --no-cache -t hapi_server_image:1.0 .
$ docker build --no-cache -t hapi_server_image:1.0 -f Dockerfile ../


# run the docker image using port 8080 for the apache2 server
$ docker run --name hapi_server -d -p 8080:80  -v .:/var/www/knmi-hapi-server/external hapi_server_image:1.0

# check the running docker processes
$ docker ps -a

# print out the authorisation key
$ docker exec hapi_server cat /var/www/knmi-hapi-server/hapi_server.key

# login to the docker for debugging
$ docker exec -t -i hapi_server /bin/bash

# test the server
$ curl http://localhost:8080/hapi/about

# setup postgres docker
$ docker run --name postgres_database -e POSTGRES_PASSWORD=hapi -p 5432:5432 -d postgres

# create database in postgres
$ psql -h localhost -p 5432 -U postgres

> CREATE DATABASE hapi_server;
> <ctrl>-d

# alternatively docker-compose can be used to start the knmi-hapi-server / postgresql / db init script
$ docker-compose up
# wait 15 seconds for the db init script to complete
$ curl localhost:8888/hapi/about