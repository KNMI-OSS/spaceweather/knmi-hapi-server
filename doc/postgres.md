Setup a local postgres database server

# path where the database data should be stored
$ export DB_PATH=/path/to/pgsql

# initialize the database server
$ initdb $DB_PATH

# change unix_socket_directories = '/tmp'
$ vi $DB_PATH/postgresql.conf

# start the server
$ pg_ctl -D $DB_PATH -l logfile start

# create the database 
$ createdb -h /tmp hapi

# query database from command line
$ psql -h /tmp hapi

