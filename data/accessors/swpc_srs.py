import pandas as pd
import os


def find_srs_file(date, basedir='.'):
    t = pd.to_datetime(date) - pd.to_timedelta(1, 'D')
    year = t.year
    ymd = t.strftime("%Y%m%d")
    filename = f'{basedir}/{year}/{year}_SRS/{ymd}SRS.txt'
    if os.path.isfile(filename):
        return filename
    filename = f'{basedir}/{year}/SRS/{ymd}SRS.txt'
    if os.path.isfile(filename):
        return filename
    return None

def parse_plage_region_lines(lines):
    list = []
    for line in lines:
        if "Nmbr" in line:
            continue
        else:
            data = line.split()
            location = data[1]
            if location[0:1] == 'N':
                lat = int(location[1:3])
            else:
                lat = -1 * int(location[1:3])
            if location[3:4] == 'E':
                lon = int(location[4:6])
            else:
                lon = -1 * int(location[4:6])
            list.append({'number': int(data[0]), 'lat': lat, 'lon': lon, 'lon_car': int(data[2])})
    return list

def parse_sunspot_region_lines(lines):
    # Nmbr Location  Lo  Area  Z   LL   NN Mag Type
    list = []
    for line in lines:
        if "Nmbr" in line:
            continue
        else:
            data = line.split()
            location = data[1]
            if location[0:1] == 'N':
                lat = int(location[1:3])
            else:
                lat = -1 * int(location[1:3])
            if location[3:4] == 'E':
                lon = int(location[4:6])
            else:
                lon = -1 * int(location[4:6])
            list.append({'number': int(data[0]), 'lat': lat, 'lon': lon, 'lon_car': int(data[2]),
                         'area': int(data[3]), 'Z': data[4], 'LL': data[5], 'NN': data[6], 'Mag type': data[7]})
    return list

def parse_srs_file(filename):
    # Set up lists and parsing switches
    sunspot_region_lines = []
    plage_region_lines = []
    sunspot_region_section = False
    plage_region_section = False
    with open(filename, 'r') as fh:
        for line in fh:
            if "Regions with Sunspots" in line:
                sunspot_region_section = True
                continue
            if "H-alpha Plages without Spots" in line:
                plage_region_section = True
                sunspot_region_section = False
                continue
            if "Regions Due to Return" in line:
                plage_region_section = False
                sunspot_region_section = False
                continue
            if sunspot_region_section:
                sunspot_region_lines.append(line)
            if plage_region_section:
                plage_region_lines.append(line)

    sunspot_regions = parse_sunspot_region_lines(sunspot_region_lines)
    plage_regions = parse_plage_region_lines(plage_region_lines)
    return(sunspot_regions, plage_regions)

def parse_srs_for_date(date, basedir='.'):
    filename = find_srs_file(date, basedir='.')
    return parse_srs_file(filename)
