import os
import configparser
import get_hapi_session_cookies
import json
import requests
import numpy as np
import pandas as pd
import gzip
import re
import xarray as xr
import glob

class Atmden():
    def __init__(self,
                 t0=pd.Timestamp.utcnow()-pd.to_timedelta(10, 'D'),
                 t1=pd.Timestamp.utcnow(),
                 base_url='https://sst-atm.spaceweatherservices.com/files/',
                 local_storage='/Volumes/datasets/atmden/',
                 configfile='./swe_auth.txt',
                 datatype='DAY_1-3'):

        config = configparser.ConfigParser()
        config.read(configfile)
        self.username = config['DEFAULT']['username']
        self.password = config['DEFAULT']['password']

        if datatype not in ['DAY_1-3', 'DAY_1-27', 'MONTH_12']:
            raise ValueError('Unknown datatype')
        self.datatype = datatype
        self.local_storage = local_storage
        self.base_url = base_url
        self.list_of_files = None
        self.t0 = t0
        if self.t0 < pd.to_datetime("2017-10-16", utc=True):
            self.t0 = pd.to_datetime("2017-10-16", utc=True)
        self.t1 = t1

    # https://sst-atm.spaceweatherservices.com/files/DAY_1-3/2022/02/ATM_DAY_1-3_F_20220211T0000Z+P09H.dat.gz
    # https://sst-atm.spaceweatherservices.com/files/DAY_1-27/2022/02/ATM_DAY_1-27_F_20220211T0000Z+P03D.dat.gz
    # https://sst-atm.spaceweatherservices.com/files/MONTH_12/2022/02/ATM_MONTH_12_P_20220211T0000Z.dat.gz

    def filelist(self):
        if self.list_of_files is not None:
            return self.list_of_files

        periods = {
            'DAY_1-3': [f'P{i:02d}H' for i in range(3, 73, 3)],
            'DAY_1-27': [f'P{i:02d}D' for i in range(1, 28, 1)],
            'MONTH_12': [""],
        }
        datatype_letter = {
            'DAY_1-3': "F",
            'DAY_1-27': "F",
            'MONTH_12': "P",
        }
        freq = {
            'DAY_1-3': "3H",
            'DAY_1-27': "1D",
            'MONTH_12': "1D",
        }

        date_range = pd.date_range(self.t0, self.t1, freq=freq[self.datatype])
        filelist = []
        for date in date_range:
            for period in periods[self.datatype]:
                if period.startswith("P"):
                    filename = f'ATM_{self.datatype}_{datatype_letter[self.datatype]}_{date.strftime("%Y%m%dT%H%M")}Z+{period}.dat.gz'
                else:
                    filename = f'ATM_{self.datatype}_{datatype_letter[self.datatype]}_{date.strftime("%Y%m%dT%H%M")}Z.dat.gz'
                path = f'{self.datatype}/{date.year}/{date.month:02d}/'
                local_subdir = f'{self.local_storage}' + path
                url = self.base_url + path + filename
                local_filename = f'{local_subdir}{filename}'
                filelist.append({
                    'url': url,
                    'local_subdir': local_subdir,
                    'local_filename': local_filename,
                    'exists': os.path.isfile(local_filename),
                    'forecast_ref_time': date + pd.to_timedelta(0, 'H'),
                    'forecast_period_time': date + pd.to_timedelta(period),
                    'forecast_period_offset': period,
                })
        self.list_of_files = filelist
        return self.list_of_files

    def download(self, overwrite=False):
        session_established = False
        filelist = self.filelist()

        # Create local subdirectories
        df_filelist = pd.DataFrame(filelist)
        local_subdirs = df_filelist['local_subdir'].unique()
        for local_subdir in local_subdirs:
            if not os.path.isdir(local_subdir):
                print(f"Creating {local_subdir}")
                os.makedirs(local_subdir)

        # Download missing files
        for item in filelist:
            if (not item['exists']) or overwrite:
                url = item['url']
                local_filename = item['local_filename']
                try:
                    if not session_established:
                        session_established, auth_cookie = get_hapi_session_cookies.get_auth_cookie(self.username, self.password)
                    response = requests.get(url, cookies = {'iPlanetDirectoryPro': auth_cookie})
                    if response.ok:
                        with open(local_filename, 'wb') as fh:
                            fh.write(response.content)
                            print(f"Downloading from {url}", end='\r')
                    else:
                        print(f"Failed downloading from {url}. {response.status_code}: {response.reason}", end='\n')

                except Exception as e:
                    print("Exception: ", e)

        # Rebuild filelist (inefficiently at the moment)
        self.list_of_files = None
        self.filelist()

    def parse_atmden_file(self, filename):
        if filename.endswith('.gz'):
            with gzip.open(filename, 'r') as fh:
                lines = fh.readlines()
        else:
            with open(filename, 'r') as fh:
                lines = fh.readlines()

        data = []
        attrs = {}
        attrs['source_file'] = filename
        for line in lines:
            if type(lines[0] == bytes):     # Make sure the content is a string
                line = line.decode('utf-8')
            if line.startswith("#") and ":" in line:
                label, value = line.strip().split(":", maxsplit=1)
                try:
                    match label.strip():
                        case '# Forecast Period DateTime (T)':
                            attrs['forecast_period_time'] = pd.to_datetime(value, utc=True)
                        case '# Forecast Reference DateTime (T0)':
                            attrs['forecast_ref_time'] = pd.to_datetime(value, utc=True)
                        case '# Input Solar Index (DateTime)':
                            solar_index, datetime_solar_index = value.strip().split(" ", maxsplit=1)
                            attrs['solar_index'] = float(solar_index)
                            attrs['datetime_solar_index'] = pd.to_datetime(datetime_solar_index[1:-1], utc=True)
                        case '# Input Solar Index 81-day avg (DateTime)':
                            solar_index81, datetime_solar_index81 = value.strip().split(" ", maxsplit=1)
                            attrs['solar_index81'] = float(solar_index81)
                            attrs['datetime_solar_index81'] = pd.to_datetime(datetime_solar_index81[1:-1], utc=True)
                        case '# Input Geo Index  (DateTime)':
                            geo_index, datetime_geo_index = value.strip().split(" ", maxsplit=1)
                            attrs['geo_index'] = float(geo_index)
                            attrs['datetime_geo_index'] = pd.to_datetime(datetime_geo_index[1:-1], utc=True)
                        case '# Input Geo Index 24-hr avg (DateTime)':
                            geo_index24, datetime_geo_index24 = value.strip().split(" ", maxsplit=1)
                            attrs['geo_index24'] = float(geo_index24)
                            attrs['datetime_geo_index24'] = pd.to_datetime(datetime_geo_index24[1:-1], utc=True)
                        case '# Array shape':
                            dimensions = list(map(str.lower, re.findall(r'\[(.*?)\]', value)[0].split(", ")))
                            shapestr = re.findall(r'\((.*?)\)', value)[0]
                            shape = tuple(np.array(shapestr.split(', ')).astype(int))
                        case '# Angular resolution (deg)':
                            attrs['angularres'] = int(value)
                        case '# Longitude range (deg)':
                            minlon = int(value.strip()[1:-1].split()[0])
                            maxlon = int(value.strip()[1:-1].split()[2])
                        case '# Latitude range (deg)':
                            minlat = int(value.strip()[1:-1].split()[0])
                            maxlat = int(value.strip()[1:-1].split()[2])
                        case '# Altitudes (km)':
                            altitudes = list(map(float, value.strip()[1:-1].split(', ')))
                        case '# Altitude level (km)':
                            pass
                        case '# Product Code':
                            pass
                        case default:
                            print("Unknown line while parsing atmden file: ", filename, label, value)
                except ValueError:
                    print("ValueError in ", filename)
            if not line.startswith("#"):
                data_section = list(map(float, line.split()))
                data.extend(data_section)

        lons = np.arange(minlon, maxlon+0.1, attrs['angularres'])
        lats = np.arange(minlat, maxlat-0.1, -1*attrs['angularres'])
        coords = {'ref_time': [attrs['forecast_ref_time'].tz_convert(None)],
                  'time': [attrs['forecast_period_time'].tz_convert(None)],
                  'latitude': lats,
                  'longitude': lons,
                  'altitude': altitudes}

        xrdata = xr.DataArray(np.array(data).reshape((1, 1, *shape)),
                              coords=coords,
                              dims=['ref_time', 'time', 'altitude', 'latitude', 'longitude'],
                              attrs=attrs)

        return xrdata


t0=pd.to_datetime('2022-01-01', utc=True)
t1=pd.to_datetime('2022-03-01', utc=True)
atmden_obj = Atmden(datatype='DAY_1-3', local_storage = os.environ['HOME'] + '/tmp/datasets/atmden/', t0=t0, t1=t1)

filelist = atmden_obj.filelist()
atmden_obj.download()

xrs = []
for file in filelist:
    print(file['local_filename'], end='\r')
    try:
        xrsfiledata = atmden_obj.parse_atmden_file(file['local_filename'])
        xrs.append(xrsfiledata)
    except FileNotFoundError:
        print(file['local_filename'], " not found.")
        pass

data = []
for xrdata in xrs:
    d = {'ref_time': xrdata['ref_time'].data[0],
         'time': xrdata['time'].data[0],
          'min': xrdata.sel(altitude=200).min(dim=['latitude','longitude']).data[0][0],
          'mean': xrdata.sel(altitude=200).mean(dim=['latitude','longitude']).data[0][0],
          'max': xrdata.sel(altitude=200).max(dim=['latitude','longitude']).data[0][0]}
    data.append(d)

df = pd.DataFrame(data)
df.index = df['time']
