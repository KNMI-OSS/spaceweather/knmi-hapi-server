import yaml
import os

def loadConfig():
    basename = 'hapi_server.cfg'
    config = {}
    config_file = 'basename'
    if not os.path.isfile(config_file):
        config_file = (os.path.dirname(os.path.realpath(__file__)) + '/../' + basename)
        if not os.path.isfile(config_file):
            config_file = os.path.expanduser('~/.' + basename)
    if os.path.isfile(config_file):
        config = yaml.safe_load(open(config_file))
        # overwrite with os.environ values
        config = {**config, **os.environ}

        return config, config_file
    else:
        raise FileNotFoundError('No configuration file found')

config, config_file = loadConfig()
