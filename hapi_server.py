#!/usr/bin/env python3
from hapi.endpoints_flask import application
import logging

from hapi import hapi_3_1_0 as hapi
#from hapi import hapi_2_1_1 as hapi

if __name__ == "__main__":

    format = '%(asctime)s [%(levelname).1s] %(message)s'
    logging.basicConfig(format=format, level=logging.INFO, datefmt='%Y-%m-%d %H:%M:%S')

    print('')
    print('KNMI HAPI Server (HAPI version ' + hapi.hapi_version + ')')
    print('server version 1.1.0 (2023-12-21)')
    print('')

    if hasattr(hapi, 'init_db'):
        hapi.init_db()

    if hasattr(hapi, 'add_about'):
        hapi.add_about()

    if hasattr(hapi, 'add_authorization'):
        hapi.add_authorization()
        print('')

    host = hapi.config['flask_server_host']
    port = hapi.config['flask_server_port']
    debug = hapi.config['flask_server_debug']

    application.run(host=host, port=port, debug=debug, use_reloader=False, threaded=True)
